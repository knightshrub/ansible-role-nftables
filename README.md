Ansible Role: nftables
======================

Set up a simple dual-stack firewall on a Linux host using nftables.

The default ruleset and configuration template is suitable for simple networking set ups with a host with a single (dual-stack) WAN interface (like a small VPS or workstation) used to host services.

Requirements
------------

None.

Role Variables
--------------

Available variables are listed below, along with their default values (see `defaults/main.yml`):

```yaml
wan_interface: "{{ ansible_facts.default_ipv4.interface }}"
nftables_allowed_dports:
  tcp:
    - ssh
nftables_dstnat: []
```
`wan_interface` is the name of the primary or WAN interface of the server or workstation. It defaults to the interface with the IPv4 default route as given by `ansible_facts`.
The variable `$wan_interface` is also defined in `nftables.conf` and
can be used in user rules (see below).
`nftables_allowed_dports` is a dictionary that can contain the keys `tcp` and `udp`. Under the respective keys follows a list of port numbers or service names as defined in `/etc/services`, traffic with this protocol/port combination will be allowed and added to the input chain.
The following configuration allows SSH on tcp/22 and mDNS on udp/5353
```yaml
nftables_allowed_dports:
  tcp:
    - ssh
  udp:
    - mdns
```

The `nftables_dstnat` list allows configuration of DNAT (sometimes called port forwarding) for specific protocol/port combinations. This is useful for running a rootless podman container that can not bind to the HTTP/HTTPS ports 80 and 443 respectively due to lack of root privileges.
In this case the container can be configured to listen on `localhost:8080` and `localhost:8443` and then incoming connections to tcp/80 and tcp/443 are DNATed accordingly:
```yaml
nftables_dstnat:
  tcp:
    - src: http
      dst: 8080
    - src: https
      dst: 8443
```
The DNAT rules are applied in the prerouting chain for all packets entering on the interface defined in `$wan_interface`.
The input chain will accept all packets that were modified by the DNAT rule.

User rules
----------

In order to make the role more flexible, the user can provide nftables configuration templates to add their own rules into the `user_input`, `user_forward`, `user_output`, `user_srcnat` and `user_dstnat` chains as well as define variables in `nftables.conf`.
The role looks for templates in `templates/{{ inventory_hostname }}/nftables/user_{input, forward, output, srcnat, dstnat, define}.j2`
Check `templates/nftables.conf.j2` to understand where rules from different templates get added in the firewall.

Dependencies
------------

None.

License
-------

BSD
